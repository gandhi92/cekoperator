package gandhi.lab.cekoperator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText no = findViewById(R.id.phone);
        Button button = findViewById(R.id.button);
        final TextView hasil = findViewById(R.id.operator);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String noHp = no.getText().toString();
                if (noHp.length() > 4){
                    // jika panjang karakter lebih dari 4
                    if (noHp.substring(0,1).equalsIgnoreCase("0")){
                        // jika karakter pertamanya adalah 0
                        // maka replace angka 0 tersebut dengan 62
                        noHp = noHp.replace("0","62");
                    }
                    if (noHp.contains("6288")){
                        noHp = noHp.substring(0,4);
                    }
                    else{
                        noHp = noHp.substring(0,5);
                    }
                }
                hasil.setText(cekOp(noHp));
            }
        });
    }

    String cekOp(String pref){
        String ret ="";
        Map<String, String> operator = getData();
        for (Map.Entry<String,String> entry : operator.entrySet()){
            entry.getKey();
            entry.getValue();
            if (pref.equalsIgnoreCase(entry.getKey())){
                ret = entry.getValue();
            }
        }
        if (ret.equalsIgnoreCase("")){
            ret = "Tidak ditemukan";
        }
        return ret;
    }

    Map<String, String> getData(){
        Map<String,String> operator = new HashMap<>();
        operator.put("62811","Telkomsel");
        operator.put("62812","Telkomsel");
        operator.put("62813","Telkomsel");
        operator.put("62821","Telkomsel");
        operator.put("62822","Telkomsel");
        operator.put("62823","Telkomsel");
        operator.put("62851","Telkomsel");
        operator.put("62852","Telkomsel");
        operator.put("62853","Telkomsel");
        operator.put("62814","Indosat");
        operator.put("62815","Indosat");
        operator.put("62816","Indosat");
        operator.put("62855","Indosat");
        operator.put("62856","Indosat");
        operator.put("62857","Indosat");
        operator.put("62858","Indosat");
        operator.put("62817","XL / Axis");
        operator.put("62818","XL / Axis");
        operator.put("62819","XL / Axis");
        operator.put("62859","XL / Axis");
        operator.put("62877","XL / Axis");
        operator.put("62878","XL / Axis");
        operator.put("62879","XL / Axis");
        operator.put("62831","XL / Axis");
        operator.put("62833","XL / Axis");
        operator.put("62835","XL / Axis");
        operator.put("62836","XL / Axis");
        operator.put("62837","XL / Axis");
        operator.put("62838","XL / Axis");
        operator.put("62839","XL / Axis");
        operator.put("6288","Smartfren");
        operator.put("62899","Three");
        operator.put("62898","Three");
        operator.put("62897","Three");
        operator.put("62896","Three");
        operator.put("62895","Three");
        operator.put("62894","Three");
        operator.put("62893","Three");
        return operator;
    }

}
